<?php
namespace uMod\Evaluator;

class PluginInfo {
    public $title;
    public $className;
    public $version;
    public $author;
    public $namespace;
    public $description;
}